﻿using CloudWorks.Store.Interfaces;

namespace CloudWorks.Store.BaseClasses
{
    public abstract class Person : IPrintable
    {
        protected Person(string name, double checkoutSpeed)
        {
            Name = name;
            CheckoutSpeed = checkoutSpeed;
        }
        public string Name { get; private set; }

        public double CheckoutSpeed { get; private set; }
        public abstract void Print();
    }
}
